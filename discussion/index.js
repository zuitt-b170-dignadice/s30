/* 
    What is an ODM 

    Object Document Mapper - is a tool that translates objects in code to documents for use in document based databases such as mongoDB (collections)
    
    odm lbirary that manages data relationships, validates schemas and simplifies MOngodb document manuipulation via the use of models

    Schemas 
    Data Presistence via Mongoose ODM 

    A schema is a representation of a documents structure, it also contains a document's expected properties and data types.

    Models = 
    a programming interface fro querying or manipulating a database. A mongoose model contains methods that simplify such operations.

    npm init -y - skipps all the prompts
    npm install express
    npm install mongoose

*/

/* 
    create an express varirable that acceps a require ("express") value, store the "express()" function inside the "app" variable

    create a port variable that accepsp 3000 

    make your app able to read json as well as accept data from forms

    make your app listen/run to the port varable with a confirmation in the console "server is running at port"
*/

const express = require("express");

// mogoose - a package that allows creation of schemas to model the data structure and to have an access to a number of methods for manipulating the concrete database in out MondoDB
const mongoose = require("mongoose");

const app = express();

const port = 3000;

// <username> change into the correct username in your MOngoDB account tghat has ADMIN functon

// <password> change into the password of the admin

// "myFirstDatabase" - the name of the database that will be created

mongoose.connect("mongodb+srv://admin:admin@cluster0.h8y6g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
})

// notification for conenction : success/failure
let db = mongoose.connection;

// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));

// to check if the conneciton is successful

db.once("open", () => console.log("we're connected to the database"))

app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Mongoose Schema - sets the structure of the documetn that is to be created; serves as the blue print to the data/record

const taskSchema = new mongoose.Schema( 
    {
        taskName : String,
        taskUserName : String,
        taskPassword : String,
        status : {
            type: String,
            // default - sets the value once the field does not have any value entered in it
            default : "pending"
        }
    }
) 

//  model - allows access to methods that will perform CRUD operations in the database 

/* 
    RULE : 
        the first letter is always uppercase for the variable of the model and must be singular form
*/

/* 
    SYNTAX : 
    const <variable> = mongoose.model("<variable>", <schemaName>)

    
*/

const Task = mongoose.model("Task", taskSchema)

// routes 
/* 
    business logic : 
        1. Check if the task is already exising
            if the task exists, return "there is a duplicate task"
            if it is not existing, add the task in the database.

        2. The task will come form the request body

        3. create a new task objeact witht hte needed properties.

        4. Save to db
*/

app.post('/tasks', (req, res) => {
    // checking for duplicate tasks 
    // findOne is a mongoose method that acts similar to find in MongoDB; returns the first document it finds
    Task.findOne({taskName:req.body.taskName}, (error, result) => {
        // if the "result" has found a task, it will return the res.send
        if(result !== null && result.taskName === req.body.taskName){
            return res.send("There is a duplicate task")
        }else {
            let newTask = new Task ({
                taskName: req.body.taskName 
            })
            // saveErr - parameter that accespts erros, should there be any when saving the "newTask" object.
            // savedTask - parameter that accepts the object should the saving of the "snewTask" object is a success.
            newTask.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr)
                } else  {
                    // .status - returns a status (number code such as 201 for successful creation)
                    return res.status(201).send("New Task Created")
                }
            })
        }
    })
})

app.get()


// business logic for retrieging data ++

/* 
1. Retrieve all the documents using th efind() functionality
2. If an error is encoutnered, print the error (error handling)
3. If there are no errors, send a success status back to the cient and return an array of documents
*/


/* 
app.get("/tasks", (req, res) => {
    Task.find((error, task) => error ? res.status(401).send(error) : res.status(200).send(task))
}) 
*/

app.get("/tasks", (req, res) => {
    // find is similar to the mongodb/robo3t find. setting up empty field in "{}" would allow the app to find all docuiments inside the database  
    Task.find( {}, (error, result) => {
        if (error){
            return console.log(error)
        } else{
            return res.status(200).json({data:result}) // data field will be set and its value will be the result/response that we had for the request.
        }
    } )
})
/* 
npm install nodemon --global'

nodemon automatically restarts the serve
*/


// create a function/router for a user registration
/* 
1. Find if there is a duplicate username/user using the username field. 
    - if the user is existing, return "username already in use"
    - if the user is not existing, add it in the database 
        -- if the usernama and the password are fileld, save
            - ins saving, create a new user object (a user schema)
            - save the user and send the response 'successfully registered"
                - if there is an error, log it in the console
                - if there is no error, send a status of 201 and "successfull registered"
    -- if one of the mis blank, sent the response "both usernama and password must be provided"
*/
// greate a get request for getting all registered users


// create a get request for getting all regsitered users
/* 
    - retrieve all users registered using find functionality
    - if there is an error, log it in t, send a success status. 
*/



app.post('/signup', (req, res) => {
    Sign.findOne({taskUserName:req.body.taskusername, taskPassword:req.body.password}, (error, result) => {
        if(result !== null && result.taskusername === req.body.taskusername){
            return res.send("Username already in use")
        }else {
            let newTask = new Sign ({
                taskUserName: req.body.taskusername,
                taskPassword: req.body.password
            })
            newTask.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr)
                } else  {
                    return res.status(201).send("Successfully Registered")
                    
                }
            })
        }
    })
})

app.get('/signup', (req, res) => {
    Task.findOne({taskUserName:req.body.taskusername, taskPassword:req.body.password}, (error, result) => {
        if(result !== null && result.taskusername === req.body.taskusername){
            return res.send("Username already in use")
        }else {
            let newTask = new Task ({
                taskUserName: req.body.taskusername,
                taskPassword: req.body.password
            })
            newTask.save((saveErr, savedTask) => {
                if (saveErr){
                    return console.error(saveErr)
                    
                } else  {
                    return res.status(201).send("Successfully Registered")
                    
                }
            })
        }
    })
})




app.listen(
    port, () =>
        console.log(`Server is running at port ${port}`)
)



